import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../guards/auth.guard';
import { DragDropComponent } from './admin/drag-drop/drag-drop.component';
import { TableFilterComponent } from './admin/table-filter/table-filter.component';
import { PagesComponent } from './pages.component';


const routes: Routes = [
  

{
  path:'admin',
  component:PagesComponent,
  children:[
    {
      path:'',
      component:TableFilterComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'postulaciones',
      component:DragDropComponent,
      canActivate:[AuthGuard]
    }  
  ]
  
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
