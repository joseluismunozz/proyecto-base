import { Component, OnInit } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  CdkDrag,
  transferArrayItem
} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-drag-drop',
  templateUrl: './drag-drop.component.html',
  styleUrls: ['./drag-drop.component.css']
})
export class DragDropComponent {

public allFiles:File[] = []

droppedFiles(allFiles: File[]): void {
  const filesAmount = allFiles.length;
  for (let i = 0; i < filesAmount; i++) {
    const file = allFiles[i];
    this.allFiles.push(file);
  }
}
  

}
