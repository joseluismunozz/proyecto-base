import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableFilterComponent } from './table-filter/table-filter.component';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { SliderModule } from 'primeng/slider';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { FormsModule } from '@angular/forms';
import { DragDropComponent } from './drag-drop/drag-drop.component';
import {FileUploadModule} from 'primeng/fileupload';



@NgModule({
  declarations: [
    TableFilterComponent,
    DragDropComponent
  ],
  imports: [
    CommonModule,
    SliderModule,
		DialogModule,
		MultiSelectModule,
		ContextMenuModule,
		DropdownModule,
		ButtonModule,
		ToastModule,
    TableModule,
    CalendarModule,
    FormsModule,
    FileUploadModule
  ],
  exports:[
    TableFilterComponent,
    SliderModule,
		DialogModule,
		MultiSelectModule,
		ContextMenuModule,
		DropdownModule,
		ButtonModule,
		ToastModule,
    TableModule,
    CalendarModule,
    FileUploadModule
  ]})
export class AdminModule { }
